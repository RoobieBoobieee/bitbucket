<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitbucketRepositoriesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('bitbucket_repositories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->enum('scm', ['git', 'hg']);
      $table->string('website')->nullable();
      $table->boolean('has_wiki');
      $table->string('uuid');
      $table->enum('fork_policy', ['allow_forks', 'no_public_forks', 'no_forks']);
      $table->string('name');
      $table->string('project_key')->nullable()->default(null);
      $table->string('project_type')->nullable()->default(null);
      $table->string('project_uuid')->nullable()->default(null);
      $table->string('project_name')->nullable()->default(null);
      $table->string('owner_uuid');
      $table->string('language');
      $table->dateTime('bb_created_on');
      $table->string('full_name');
      $table->boolean('has_issues');
      // $table->string('owner'); // TODO
      $table->datetime('bb_updated_on');
      $table->integer('size');
      $table->string('type');
      $table->string('slug');
      $table->boolean('is_private');
      $table->longtext('description');
      $table->string('checksum', 32);
      $table->string('last_commits_page')->nullable()->default(null);
      $table->timestamps();

      $table->unique('uuid');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('bitbucket_repositories');
  }
}
