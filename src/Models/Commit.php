<?php

namespace RoobieBoobieee\Bitbucket\Models;

use Illuminate\Database\Eloquent\Model;
use RoobieBoobieee\Bitbucket\Interfaces\Syncable;
use RoobieBoobieee\Bitbucket\Models\Repository;

class Commit extends Model implements Syncable
{
  protected $table = 'bitbucket_commits';

  protected $fillable = [
    'hash',
    'repository_uuid',
    'author_user_uuid',
    'bb_date',
    'message',
  ];

  public static function primaryKey()
  {
    return 'hash';
  }

  public function repository()
  {
    return $this->hasOne(Repository::class, 'uuid', 'repository_uuid');
  }

  public function saveLastPage($page)
  {
    $this->repository->last_commits_page = $page;
    return $this->repository->save();
  }

}
