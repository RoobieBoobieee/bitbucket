# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/roobieboobieee/bitbucket.svg?style=flat-square)](https://packagist.org/packages/roobieboobieee/bitbucket)
[![Build Status](https://img.shields.io/travis/roobieboobieee/bitbucket/master.svg?style=flat-square)](https://travis-ci.org/roobieboobieee/bitbucket)
[![Quality Score](https://img.shields.io/scrutinizer/g/roobieboobieee/bitbucket.svg?style=flat-square)](https://scrutinizer-ci.com/g/roobieboobieee/bitbucket)
[![Total Downloads](https://img.shields.io/packagist/dt/roobieboobieee/bitbucket.svg?style=flat-square)](https://packagist.org/packages/roobieboobieee/bitbucket)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require RoobieBoobieee/Bitbucket
```


```php
<?php
// config/app.php

// ...
RoobieBoobieee\Bitbucket\BitbucketServiceProvider::class,
// ...
```

## Usage

Start a full sync job:
``` php
<?php
use RoobieBoobieee\Bitbucket\Jobs\SyncBitbucket;
//...
SyncBitbucket::dispatch(config('services.bitbucket.username'), config('services.bitbucket.password'));
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email robvankeilegom@gmail.com instead of using the issue tracker.

## Credits

- [Rob Van Keilegom](https://github.com/roobieboobieee)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
